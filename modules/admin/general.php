<?php
if (!defined('SPEEDCMS')) { exit(1);}
/**
 * 全部控制器页面的父类
 * 
 * 实现一些全局的页面显示
 * @author Harrie
 * @version 1.0
 * @created 2010-06-28
 */
class general extends spController
{
	/**
	 * 默认风格
	 */
	var $themes = "default";
	
	/**
	 * 站点配置
	 */
	var $defined = "";
	
	/**
	 * 当前页面title
	 */
	var $title = "";
	
	/**
	 * 侧栏显示着谁的资料呢？
	 */
	var $sidebar_username = "";
	
	/**
	 * 覆盖控制器构造函数，进行相关的赋值操作
	 */
	public function __construct()
	{
		parent::__construct();
	}
	
	
	/**
	 * 错误提示程序  应用程序的控制器类可以覆盖该函数以使用自定义的错误提示
	 * @param $msg   错误提示需要的相关信息
	 * @param $url   跳转地址
	 * 
	 * @param msg
	 * @param url
	 */
	public function jsonerror($msg, $url='')
	{
		exit("{'status': 0, 'data': {".$msg."},'url': '".$url."'}");
	}

	/**
	 * 成功提示程序  应用程序的控制器类可以覆盖该函数以使用自定义的成功提示
	 * @param $msg   成功提示需要的相关信息
	 * @param $url   跳转地址
	 * 
	 * @param msg
	 * @param url
	 */
	public function jsonsuccess($msg, $url)
	{
		exit("{'status': 1, 'msg': '".$msg."', 'url':'".$url."'}");
	}

	/**
	 * 错误提示程序  应用程序的控制器类可以覆盖该函数以使用自定义的错误提示
	 * @param $msg   错误提示需要的相关信息
	 * @param $url   跳转地址
	 * 
	 * @param msg
	 * @param url
	 */
	/*public function error($msg, $url)
	{
		$this->msg = $msg;
		$this->url = $url;
		$this->display("error.html");
		exit();
	}*/

	/**
	 * 成功提示程序  应用程序的控制器类可以覆盖该函数以使用自定义的成功提示
	 * @param $msg   成功提示需要的相关信息
	 * @param $url   跳转地址
	 * 
	 * @param msg
	 * @param url
	 */
	/*public function success($msg, $url)
	{
		$this->msg = $msg;
		$this->url = $url;
		$this->display("success.html");
		exit();
	}*/

}
?>