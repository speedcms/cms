<?php
define('SPEEDCMS', true);
// 后台模块程序入口文件

// 载入配置与定义文件
require("config.php");

// 当前模块附加的配置
$spConfig['controller_path'] = APP_PATH.'/modules/'.basename(__FILE__,".php");

$spConfig['launch']['router_prefilter'] =  array( 
	//array('spAcl','mincheck') // 开启有限的权限控制
	array('spAcl','maxcheck') // 开启强制的权限控制
);
	 
$spConfig['ext']['spAcl'] = array( // acl扩展设置
	// 在acl中，设置无权限执行将lib_user类的acljump函数
	'prompt' => array("userModel", "acljump"),
);

// 载入SpeedPHP框架
require(SP_PATH."/SpeedPHP.php");
spRun();