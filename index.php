<?php
define('SPEEDCMS', true);

define("APP_PATH",dirname(__FILE__));
define("SPEEDCMS_VERSION","0.1.3");
if( true != @file_exists(APP_PATH.'/config.php') ){require(APP_PATH.'/install.php');exit;}
// 网站主体模块程序入口文件

// 载入配置与定义文件
require("config.php");

// 当前模块附加的配置
$spConfig['controller_path'] = APP_PATH.'/modules/'.basename(__FILE__,".php");

// 载入SpeedPHP框架
require(SP_PATH."/SpeedPHP.php");
spRun();